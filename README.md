# README #

This repository can be used by team SYNAPSE to upload their source code

### What is this repository for? ###

This repository holds the files and folders of source code

### How do I get set up? ###

Clone the "develop" stream of the repository to your local.
Refer[https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html]
Add and push the codes to develop stream.
Create a pull request to "master" stream.


### Who do I talk to? ###

Vishnulal K J or Any volunteers present in the venue
